--
-- Build Step Class
--	Handles build step events
--	and triggering
--

-- Class
local buildstep = {}


-- Attach Function
--	Can be daisy chained
buildstep.attach = function(step, name, callback, after)
	after = after or true
	local cb = callback or error("No callback function for '" .. name .. "' when attaching to " .. tostring(step))
	local event = setmetatable({name = name}, {__call = function (t, ...) return callback(...) end})

	if after then
		table.insert(step.events, event)
	else
		table.insert(step.events, event, 1)
	end

	return step
end


-- Creation Function
buildstep.new = function(name, description, quiet)
	return setmetatable({name = name, description = description, quiet = quiet or false, events = {}}, {
		__index = buildstep,

		__tostring = function(t)
			return "LAB build step: " .. t.name
		end,

		__call = function(t, ...)
			if #t.events == 0 and not t.quiet then log.w(tostring(t) .. " has no events; nothing will be done") end
			for i,cb in ipairs(t.events) do
				local fullname = tostring(t.name) .. ":" .. tostring(cb.name)
				log.v("Executing event: " .. fullname)
				local result, errormsg = cb(...)
				if result == nil then
					log.w(fullname .. " did not return a result; assuming success")
					result = true
				end
				if not result then
					log.e("FAILURE <" .. fullname .. ">: $37" .. (errormsg or "Unknown error"))
					return false
				end
			end
			
			if not t.quiet then log.s("SUCCESS <" .. tostring(t) .. " [" .. #t.events .. "]>") end
			return true
		end
	})
end

-- Return
return buildstep
