--
-- "Course" system
--	The course system is a set of
--	default LAB events that mimic
--	Android's build system's convention
--	of naming the steps of the common
--	build cycle.
--
--	Because LAB just can't not do it,
--	it takes it a step further.
--
--	Courses include:
--	- BREAKFAST - Set up workspace
--	- LUNCH - Build workspace given configuration
--	- DINNER - Package workspace
--
--	Advanced courses include:
--	- BRUNCH - Obtain / build libraries
--	- TEA - Run [unit] tests
--	- DESSERT - Upload data/packages to server(s)
--
--	Recommended build cycle order:
--	BREAKFAST BRUNCH [ LUNCH TEA ] DINNER DESSERT
--	                 ^-----<------
--

-- Global build steps
breakfast =			lab.step.new("Breakfast",	"Sets up the workspace")
lunch =				lab.step.new("Lunch",		"Builds the workspace")
dinner =			lab.step.new("Dinner",		"Packages the workspace")
brunch =			lab.step.new("Brunch",		"Obtains / Builds libraries")
tea =				lab.step.new("Tea",			"Builds / Performs unit/target tests")
dessert =			lab.step.new("Dessert",		"Uploads data / packages to any server(s)")
