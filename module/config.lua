--
-- LAB Configuration System
--	The configuration system is simply
--	a dirty override of the tostring() method
--	to expand variables out to their stored
--	values.
--

-- Store original tostring()
rawtostring = tostring

-- Configuration registry
local configuration = {}

-- Override global indexer
if getmetatable(_G) then error("Global table already has a metatable! Another script is interfering!") end

-- Configuration inheritance function
--	This super ninja metatable editor function
--	sets up a proxy function that allows the direct
--	assignment to configuration variables assuming
--	they're defined with define().
--
--	This can be used by things that derive configuration
--	sets off of a parent configuration (such as projects)
--	safely inherit values from a parent while proxying values
--	to their own private configuration store without overwriting
--	actual configuration value objects.
function setconfigproxy(tbl, parent)
	assert(rawget(tbl, 'configuration'))	
	local mt = getmetatable(tbl) or {}

	mt.__index = function(t, k)
		if not rawequal(t.configuration[k], nil) then return t.configuration[k] end
		return parent[k]
	end

	mt.__newindex = function(t,k,v)
		if not t.configuration[k] then
			local forstring = ""
			if t ~= configuration then forstring = " for " .. tostring(t) end
			error("Configuration value '" .. k .. "' not defined" .. forstring)
		end
		t['configuration'][k].value = v
	end

	setmetatable(tbl, mt)
end

-- Configuration storage proxy metatable definition
--	*phew*
configuration.configuration = {}
setconfigproxy(configuration, configuration.configuration)

-- Configuration variable class
local configvar = {}
local configvarmt = {
	__tostring = function(t)
		return rawtostring(t.value)		
	end,

	__len = function(t)
		local val = rawtostring(t.value):gsub("^;?(.*);?$", "%1")
		if val:gsub("%s+", "%s"):len() == 0 then return 0 end
		local count = 0
		for i in rawtostring(val):gsub("^;*(.*);*$", "%1"):gmatch(";") do
			count = count + 1
		end
		return count + 1
	end,

	__index = function(t, k)
		if string[k] then return string[k] end
		return nil
	end,

	__concat = function(a, b)
		return tostring(a) .. tostring(b)
	end
}

-- Define function
function define(name, val, description, scope)
	name = name or error("Name not given when defining a configuration value")
	val = val or ""
	scope = scope or configuration
	assert(rawget(scope, "configuration") ~= nil)

	local curconf = scope["configuration"][name]
	if curconf then
		local scopestr = ""
		if scope ~= configuration then scopestr = " for " .. scope end
		log.w("Configuration variable '" .. name .. "' already defined" .. scopestr .. "; just setting")
		curconf.value = val
		return curconf
	end

	local conf = setmetatable({}, configvarmt)

	conf.value = val
	conf.description = description

	scope.configuration[name] = conf
	
	return conf
end

-- Override tostring
--	Introduces a second parameter
--	that allows the pulling of
--	a derived configuration
--	to resolve in-string configuration
--	variables
tostring = function(val, root)
	val = rawtostring(val)
	root = root or configuration
	local strlib = ___string or string
	
	while strlib.find(val, "%$%{[%w_]+%}") do
		local varname = strlib.gsub(val, "^.*%$%{([%w_]+)%}.*$", "%1")
		local confval = root[varname]
		if not confval then error("Configuration value '" .. varname .. "' not set!") end
		val = strlib.gsub(val, "^(.*)%$%{[%w_]+%}(.*)$", "%1" .. rawtostring(confval) .. "%2")
	end

	return val
end

-- Return
return configuration
