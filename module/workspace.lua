--
-- Workspace Module
--	Facilitates the discovery
--	of projects and provides a
--	mechanism for building and managing
--	them.
--

-- Local tables
local workspace = {}
local system = {}
local project = setmetatable({}, {__index = configuration})

-- Discovery Types
workspace.types = {}
workspace.projects = {}

-- SYSTEM: Discovery Pattern Getter
function system:discoverpattern()
	error("System '" .. self.name .. "' does not specify a discovery pattern")
end

-- SYSTEM: Name formatting from found project files
function system:getnamefromfile(pth)
	error("System '" .. self.name .. "' does not implement getnamefromfile()")
end

-- SYSTEM: Configure per-project (initial sync Lua -> System)
function system:configure(project)
	error("System '" .. self.name .. "' does not implement configure()")
end

-- SYSTEM: Generate per-project
function system:generate(project)
	error("System '" .. self.name .. "' does not implement generate()")
end

-- SYSTEM: Sync From (System -> Lua)
function system:syncfrom(project)
	error("System '" .. self.name .. "' does not implement syncfrom()")
end

-- SYSTEM: Sync To (Lua -> System)
function system:syncto(project)
	error("System '" .. self.name .. "' does not implement syncto()")
end

-- PROJECT: Get System
function project:getsystem()
	return self.system
end

-- PROJECT: Configure
function project:configure()
	local res, message = false, nil
	res, message = self.system:configure(self)
	if res == false then return res, message end
	res, message = self.system:syncfrom(self)
	if res == false then return res, message end
	return true
end

-- PROJECT: Generate
function project:generate()
	local res, message = false, nil 
	res, message = self.system:generate(self)
	if res == false then return res, message end
	return true
end

-- Append all configuration values to destination table
local function appendconfigs(dest, src)
	for k,v in pairs(src.configuration) do
		dest[k] = v
	end
	return dest
end

-- PROJECT: Dump Configuration
function project:dumpconfig()
	local confdump = {}

	appendconfigs(confdump, configuration)
	appendconfigs(confdump, self)

	return confdump
end

-- New System type
workspace.new = function(name)
	local sys = setmetatable({name = name}, {__index=system})
	workspace.types[name] = sys
	return sys
end

-- New Project
workspace.project = function(name, projectfile, system)
	local proj = setmetatable({
		configuration = {},
		system = system,
		prebuild = lab.step.new(name .. ":pre-build", nil, true),
		postbuild = lab.step.new(name .. ":post-build", nil, true)
		},{
			__tostring = function(t) return "project: " .. (t.PROJECT_NAME or name) end,
	
			__concat = function(a, b) return tostring(a) .. tostring(b) end
		})

	setconfigproxy(proj, project) -- Parent is project, not configuration (we manually set this earlier)

	define("PROJECT_NAME", name, "The name of the project", proj)
	define("PROJECT_FILE", Path(projectfile):abs(), "The source project file (system dependant)", proj)
	define("SYSTEM", system.name, "The name of the build system handler that is processing this project", proj)
	define("PROJECT_DIR", Path(proj.PROJECT_FILE):abs():dirname(), "The path to the directory of the project file (the source directory)", proj)
	define("BIN_DIR", Path("${BIN_DIR}/${PROJECT_NAME}", proj):abs(), "The path to the directory of the generated project files (the binary directory)", proj)
	
	breakfast:attach("configure:" .. name, function() return project.configure(proj) end, true)
	breakfast:attach("generate:" .. name, function() return project.generate(proj) end, true)

	workspace.projects[name] = proj

	return proj
end

-- Init
workspace.init = function()
	for i,pth in ipairs(Path(Path("./system"):abs(lab.workspacedir)):list(false, "system%.%w+%.lua$")) do
		pth = pth:gsub("^.+/system%.(%w+)%.lua$", "%1")
		require(pth)
	end
end

-- Discover function
workspace.discover = function(basepath)
	local pth = Path(basepath or "./")
	local cnt = 0

	for name,sys in pairs(workspace.types) do
		local projectfiles = pth:list(true, sys:discoverpattern())
		for i,pf in ipairs(projectfiles) do
			local name = sys:getnamefromfile(pf:gsub("^" .. string.quote(basepath), ""):gsub("^/", ""):gsub("/", "."))
			local project = workspace.project(name, pf, sys)
			cnt = cnt + 1
		end
	end

	return cnt
end

-- Return
return workspace
