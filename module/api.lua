--
-- API Module
--	Defines the API for build files
--	to use. Entries returned by this
--	module are merged with the main
--	`lab` table
--

-- Local table
local api = {}

-- Search function
--	Searches for project files given
--	a base directory and optionally
--	excluding specific directories.
--
--	Exclusion directories pre-pend
--	the base to them and must NOT
--	be absolute.
api.search = function (base, ...)
	local excl = {...}
	
	local cnt = lab.system.discover(base)

	for i,excldir in ipairs(excl) do
		excldir = Path(excldir)
		local isabs = not excldir:isabsolute() or error("Exclusion directories cannot be absolute: " .. excldir)

		local torem = {}
		for name, proj in pairs(lab.system.projects) do
			if proj.file:begins(tostring(excldir) .. "/") then
				table.insert(torem, name)
				cnt = cnt - 1
			end
		end
		
		lab.system.projects = table.removelist(lab.system.projects, torem)
	end

	return cnt
end

-- Return
return api
