--
-- Default Configuration
--	Sets up the default configuration
--	values for LAB
--

define(	"VERBOSITY",		"iweIs",
		"Lists the log levels to show when running")

define(	"BIN_DIR",			"./bin",
		"The base bin directory for project files/executables")
