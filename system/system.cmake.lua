--
-- CMake Build System Handler
--

-- Handler Instance
local cmake = lab.system.new("cmake")

-- Discovery Pattern
function cmake:discoverpattern()
	return "/CMakeLists%.txt$"
end

-- Name Formatting from found project file
function cmake:getnamefromfile(pth)
	return pth:gsub("%.CMakeLists%.txt$", "")
end

-- Configuration (per-project; initial syncto)
function cmake:configure(proj)
	log.I("Configuring project: " .. proj)

	log.v("Making binary directory: ${BIN_DIR}", proj)
	Path(proj.BIN_DIR):mkdirs()

	local confdump = proj:dumpconfig()
	log.v("Initial CMake cache sync: ${BIN_DIR}/CMakeCache.txt <" .. table.count(confdump) .. " variables>", proj)
	local cache = assert(io.open(tostring("${BIN_DIR}/CMakeCache.txt", proj), "w"))
	for k,v in pairs(confdump) do
		local doc = ""
		if v.description then doc = "//" .. v.description .. "\n" end
		
		local valid = true
		local tipostr = "STRING"
		local tipo = type(v.value)
		if tipo == "table" and not getmetatable(v) or not getmetatable(v).__tostring then
			if #v.value == 0 then
				valid = false
				log.w("Config table '" .. k .. "' for " .. proj .. " doesn't contain any numeric values; skipping")
			else
				local nv = tostring(v.value[1])
				for i=2,#v.value,1 do
					nv = nv .. ";" .. tostring(v.value[i])
				end
				v = nv
			end
		elseif tipo == "boolean" then
			tipostr = "BOOL"
		elseif tipo == "table" and v.value.abs then
			v = tostring(v.value:abs())
		elseif tipo == "table" and getmetatable(v.value) and getmetatable(v.value).__tostring then
			v = tostring(v.value)
		elseif tipo ~= "number" and tipo ~= "string" then
			log.w("Configuration value '" .. k .. "' is not valid for " .. proj .. "; skipping")
			valid = false
		else
			v = tostring(v.value)
		end

		if valid then
			local confentry = doc .. "LAB_" .. k .. ":" .. tipostr .. "=" .. v .. "\n\n"
			cache:write(confentry)
		end 
	end	
	io.close(cache)

	os.run(tostring([[cd "${BIN_DIR}" && cmake "${PROJECT_DIR}"]], proj))
end

-- Sync From (CMakeCache -> Lua)
function cmake:syncfrom(proj)
	-- TODO
	--	How this is going to work:
	--		If the variable starts with LAB_
	--		then simply set (if exists) or define (if it doesn't)
	--		back in the project. Otherwise, set/define
	--		the variable with prefix "CMAKE_" (if it doesn't already
	--		have the prefix; no sense in having CMAKE_CMAKE_GENERATOR)
	--
	--		Skip all lines starting with #
	--
	--		Lines with // mean a variable is coming up; add to
	--		a description buffer to be passed to define()
	--
	--		If a blank line occurs after a // but before
	--		a variable definition, error due to malformed
	--		CMakeCache.txt file.
	error("Not Implemented")
end

-- Return
return cmake
