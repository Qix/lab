--
-- String(X)
--	String extensions
--

-- String Library tostring() fix
___string = {}
for k,v in pairs(string) do
	___string[k] = v
	string[k] = function(...)
		local a = {...}
		if a[1] then a[1] = tostring(a[1]) end
		for i=2,#a do
			local ar = a[i]
			local art = type(ar)
			if art == "table" or art == "userdata" then
				local mt = getmetatable(ar)
				a[i] = (rawtostring or tostring)(ar)
			end
		end
		return v(table.unpack(a))
	end
end

-- Pattern Quote
local quotepattern = '(['..("%^$().[]*+-?"):gsub("(.)", "%%%1")..'])'
string.quote = function(str)
	str = tostring(str)
	return str:gsub(quotepattern, "%%%1")
end

-- Starts With
string.begins = function(src, comp)
	src = tostring(src)
	comp = tostring(comp)
	return src:find('^' .. comp:quote()) ~= nil
end

-- Ends With
string.ends = function(src, comp)
	src = tostring(src)
	comp = tostring(comp)
	return src:find(comp:quote() .. '^') ~= nil
end
