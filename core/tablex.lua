--
-- Table(X) core extension
--	Implements extensions to the tables
--	library
--

-- Merges all values of a child table with
--	the current table
function table.merge(src, ...)
	local tbls = {...}
	for i,tbl in ipairs(tbls) do
		for k,v in pairs(tbl) do
			src[k] = v
		end
	end

	return src
end

-- Removes all values with the keys
--	using a list of keys from the second parameter
function table.removelist(tbl, keys)
	keys = keys or {}
	for i,key in ipairs(keys) do
		tbl[key] = nil
	end
	return tbl
end

-- Count pairs + indexes
function table.count(tbl)
	assert(type(tbl) == "table")
	local cnt = 0
	for k,v in pairs(tbl) do
		cnt = cnt + 1
	end
	return cnt
end
