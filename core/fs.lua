--
-- FileSystem module
--	Core module that abstracts
--	much of the filesystem routines
--

-- Path Class
path = {}

-- Windows Slashes
path.winslashes = function(name)
	return tostring(name):gsub("/", "\\")
end

-- Unix Slashes
path.unixslashes = function(name)
	return tostring(name):gsub("\\", "/")
end

-- Slashes
path.slashes = function(name)
	if os.win32 then return path.winslashes(name)
	else return path.unixslashes(name) end
end

-- Windows Format
path.win = function(name)
	name = path.winslashes(name)
	if name:find("^\\[a-z]\\") then
		local drive = name:gsub("^\\([a-z]).*$", "%1"):upper()
		name = drive .. ":" .. name:gsub("^\\[a-z](\\.*)$", "%1")
	elseif name:find("^\\") then
		name = "C:" .. name		
	end
	return name
end

-- Unix Format
path.unix = function(name)
	name = path.unixslashes(name)
	if not name:find("^%.?/") then
		name = "./" .. name
	end
	return name
end

-- Formatter
path.format = function(name)
	if os.win32 then
		return path.win(name)
	else
		return path.unix(name)
	end
end

-- Basename
path.basename = function(name)
	name = path.unixslashes(name)
	return Path(name:gsub("^.*/([^/]+)/?$", "%1"))
end

-- Dirname
path.dirname = function(name)
	name = path.unixslashes(name)
	name = name:gsub("^(.*)/[^/]*/?$", "%1")
	if name == "" then return Path("/")
	else return Path(name) end
end

-- Expand
path.expand = function(name)
	name = path.unixslashes(name)
	local newpath = {}
	local ind = 1
	for leaf in name:gmatch("[^/]+") do
		if leaf ~= '.' then
			if leaf == '..' then
				ind = ind - 1
			else
				newpath[ind] = leaf
				ind = ind + 1
			end
		end
	end

	local newpathstr = ""
	local delim = ""
	if name:find("^%.?/") then
		delim = name:gsub("^(%.?/).*$", "%1")
	end
	for i=1,(ind-1) do
		local leaf = newpath[i]
		newpathstr = newpathstr .. delim .. leaf
		delim = "/"
	end

	return Path(newpathstr)
end

-- Move (Rename)
path.mv = os.rename

-- Delete (Remove)
path.rm = os.remove

-- Copy
path.copy = function(src, dst)
	if os.win32 then
		os.run('copy "' .. src .. '" "' .. dst .. '"')
	else
		os.run('cp -r "' .. src .. '" "' .. dst .. '"')
	end
end

-- Is absolute
path.isabsolute = function(name)
	name = path.unixslashes(name)
	return name:find("^/") ~= nil
end

-- Make dir(s)
path.mkdirs = function(name)
	if os.win32 then
		os.run("md " .. Path(name):win())
	else
		os.run("mkdir --parents " .. Path(name):unix())
	end
end

-- Make link (to)
--	Basically, make a link to this path
--	given a name of the link as the second
--	param
path.link = function(dest, name)
	dest = Path(dest)
	name = Path(name)
	local cmd = "ln -s $1 $2"
	if os.win32 then
		cmd = "mklink /J $2 $1"
	end
	
	cmd = cmd:gsub("%$1", '"'..dest:abs()..'"'):gsub("%$2", '"'..name:basename()..'"')
	os.run('cd "' .. name:dirname() .. '" && ' .. cmd)
end

-- To absolute path
path.abs = function(name, relative)
	relative = Path(relative or os.cwd)
	name = Path(name)
	if name:find("^/") then
		return name:expand()
	else
		return Path(relative .. "/" .. name):expand()
	end
end

-- List all in path
--	NOTE: Does not check if directory
path.list = function(name, recurse, ...)
	recurse = recurse or true
	local patterns = {...}
	if #patterns == 0 then
		patterns = nil
	else
		for i,v in ipairs(patterns) do patterns[i] = v end
	end
	
	local output = nil
	if os.win32 then
		local rflag = " /S"
		if not recurse then rflag = "" end
		output = os.run("dir" .. rflag .. ' /B "' .. name .. '"')
	else
		local rflag = " -maxdepth 1"
		if recurse then rflag = "" end
		output = os.run('find "' .. name .. '"' .. rflag)
	end

	local paths = {}
	for pth in output:gsub("\r", ""):gmatch("[^\n]+") do
		pth = Path(pth):expand()
		if not patterns then
			table.insert(paths, Path(pth))
		else
			local found = false
			for i,v in ipairs(patterns) do
				if pth:find(v) and not found then
					table.insert(paths, Path(pth))
					found = true
				end
			end
		end
	end
	
	return paths
end

-- Path creation function
function Path(lpathname, root)
	lpathname = tostring(rawtostring(lpathname), root) -- This MUST stay here; a new Path() object must copy internal string!
	lpathname = path.unixslashes(lpathname):gsub("/*$", "")
	if lpathname:find("^[A-Z]:/") then
		local drive = lpathname:gsub("^([A-Z]):/.*$", "/%1"):lower()
		lpathname = lpathname:gsub("^[A-Z]:(/.*)$", drive .. "%1")
	end

	return setmetatable({pathname = lpathname}, {
		__index = function(t, k)
			if path[k] then return path[k] end
			return string[k]
		end,

		__tostring = function(t)
			return path.format(t.pathname)
		end,

		__concat = function(a, b)
			return tostring(a) .. tostring(b)
		end		
		})
end
