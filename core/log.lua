--
-- Log module
--
--	Handles logging and colors
--

-- Table
local log = {}

-- UNIX ANSI color converter
log.ansicol = function(src)
	if os.win32 and not os.cygwin then
		return src:gsub("%$%d%d?", "")
	end
	local timeout = 0
	while src:find("%$%d%d?") and timeout < 100 do
		local col = src:gsub("^.*%$(%d%d?).*$", "%1", 1)
		col = "\x1b["..col.."m"
		src = src:gsub("^(.*)%$%d%d?(.*)$", "%1"..col.."%2", 1)
		timeout = timeout + 1
	end

	if timeout == 100 then error("ansicol timed out (100): " .. src) end

	return src
end

-- Set up logging level metatable
local llmt = {
	__call = function(t, msg, root)
		if VERBOSITY and not tostring(VERBOSITY, root):find(t.flag:quote()) then return end
		if os.unix then -- TODO Can this also be for Mac?
			local icon = log.ansicol(tostring(t.icon)) .. string.rep(' ', 4 - string.len(string.gsub(t.icon, "%$%d%d?", "")))
			local cmsg = log.ansicol(t.defcol .. icon .. ' ' .. tostring(msg, root):gsub("%$00?([^1-9])", t.defcol .. "%1") .. '$0\n')
			io.write(cmsg)
		else
			local icon = tostring(t.icon) .. string.rep(' ', 4 - string.len(t.icon))
			io.write(icon .. ' ' .. tostring(msg, root) .. '\n')
		end
	end
}

-- Ease-of-access function
local function level(licon, ldefcol)
	return setmetatable({icon = licon, defcol = ldefcol or "$0"}, llmt)
end

-- Wrap with VERBOSITY setter
assert(not getmetatable(log))
setmetatable(log, {
	__newindex = function(t, k, v)
		v.flag = tostring(k)
		rawset(t,k,v)
	end
})

-- Set up levels
log.i = level('')					-- Info
log.I = level('-', "$1$37")			-- Important
log.v = level('', "$1$30")			-- Verbose
log.w = level('>', "$33$1")			-- Warning
log.e = level('!->', "$1$31")		-- Error
log.s = level('*', "$1$32")			-- Success
log.d = level('~', "$36")			-- Debug

-- Override print
--	print() is not backed up
--	since we can perform io.write()
--	in its place if absolutely need be
print = log.i

-- Return
return log

