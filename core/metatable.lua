--
-- Metatable Utilities
--	Helpful little snippets
--	that concentrate on metatables
--

-- 'Traits'
--	This makes a new table (or optionally
--	modifies an existing table) to apply
--	a 'trait':
--		- The main table is A; the trait table is B
--		- If A has the value, return it (standard __index)
--		- If B has the value, return it (standard __index)
--		- When writing, if A doesn't have the key,
--			and B doesn't have that key, then write to A.
--		- When writing, if A has the key, write to A.
--		- When writing, if A doesn't have the key but
--			B does, overwrite B.
function settrait(trait, source)
	source = source or {}
	trait = trait or error("No trait specified")

	return setmetatable(source, {__index=trait, __newindex = function(t,k,v)
		local tmt = getmetatable(t)
		if tmt and tmt.__index then
			local tipo = type(tmt.__index)
			if tipo == "table" then
				if tmt.__index[k] ~= nil then
					tmt.__index[k] = v
					return v
				end
			else
				local errmsg = "settrait() doesn't support trait types other than 'table'"
				if log then
					log.w(errmsg)
				else
					print("WARNING: " .. errmsg)
				end 
			end
		end

		return rawset(t,k,v)
	end})
end
