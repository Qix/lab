--
-- OS(x) module
--	Handles OS detection and environment
--	configuration on top of the current
--	operating system functions.
--	
--	OS(X) doesn't require the use of the `fs`
--	core module, while `fs` DOES require OS(X)
--	thus OS(X) must be loaded before `fs`.
--

-- Table
local osx = {}

-- Detect operating system
osx.win32 = false
osx.unix = false
osx.macosx = false
osx.cygwin = false

--> Windows?
local file = io.open('C:/WINDOWS', 'r')
if file ~= nil then
	osx.win32 = true
end

--> Mac OSX?
if not osx.win32 then
	file = io.open('/Applications', 'r')
	if file ~= nil then
		osx.macosx = true
	end
end

--> Unix?
if not osx.macosx and not osx.win32 then
	file = io.open('/bin', 'r')
	if file ~= nil then
		osx.unix = true
	end
end

--> Unknown?
if not osx.unix and not osx.win32 and not osx.macosx then
	error ('Could not determine operating system; terminating!')
end

-- Execute + Get Output ("Run")
osx.run = function(cmd)
	return assert(io.popen(cmd)):read("*a")
end

-- Cygwin Check
local unamecheck = io.popen("uname")
if osx.win32 and unamecheck then
	io.close(unamecheck)
	if tonumber(osx.run("uname | grep -i cygwin | wc -l")) == 1 then
		osx.cygwin = true
	end
end

-- CWD
if osx.win32 then
	osx.cwd = osx.run("cd"):gsub("[\r\n]+$", "")
else
	osx.cwd = osx.run("pwd"):gsub("[\r\n]+$", "")
end

-- Return
return osx
