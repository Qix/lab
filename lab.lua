--
-- L/ua A/utomated B/uilder
--
--	by Qix
--	(C) 2013
--	for Rippl Studio
--
--	LAB is intended to be used
--	as a configuration/make tool.
--	Its intended purpose is to be
--	run outright using `lua -i`,
--	NOT by being invoked by the
--	project build scripts themselves.
--
--	LAB automatically detects scripts
--	in the CWD (regardless of from where
--	the LAB script is run) and uses them
--	to build configuration values, source
--	pools, and finally the environmental
--	setup scripts to provide the user
--	with an interactive shell to perform
--	all necessary build commands (i.e.
--	breakfast, lunch and dinner for setup,
--	build and packaging - including brunch
--	for tests).

-- lab.lua
--	Provides the core API access
--	and functionality to the
--	system and performs all necessary
--	modifications to set up the build
--	environment.

-- Global table
lab = {}

-- Setup package path
--	This allows us to automatically load
--	modules (using require()) from where
--	this script is regardless of where
--	LAB is invoked.
if arg[0] == '/lab.lua' then
	error('LAB cannot be run from root!')
end
local curscript = arg[0]:gsub("(.+/)lab.lua$", "%1")
if curscript == "lab.lua" then
	curscript = "./"
end
lab.workspacedir = curscript:gsub("/$", "")

-- Package paths
package.path = package.path .. ';' .. curscript .. '?.lua'
package.path = package.path .. ';' .. curscript .. 'core/?.lua'
package.path = package.path .. ';' .. curscript .. 'module/?.lua'
package.path = package.path .. ';' .. curscript .. 'system/system.?.lua'

-- Include core libraries
require 'stringx'						-- String extensions
require 'tablex'						-- Table extensions
require 'metatable'						-- Metatable utilities
local osx = require 'osx'				-- OS extensions
table.merge(os, osx)					--	Merge OSX into global OS table
log = require 'log'						-- Logging
local fs = require 'fs'					-- Filesystem (Paths)

-- Include LAB modules
configuration = require 'config'		-- Configuration system
settrait(configuration, _G)				--	Extend global with configuration system
require 'defaults'						-- Default configuration values
lab.step = require 'buildstep'			-- Build step system
require 'course'						-- LAB's "food course" build cycle events
lab.system = require 'workspace'		-- Workspace (Build System) framework
lab.system.init()						--	Initialize workspace system
project = lab.system.projects			--	Make projects easier to access
local api = require 'api'				-- Main LAB API
table.merge(lab, api)					--	Merge API into global LAB table


-- Modify Prompt
_PROMPT = log.ansicol("$30$1:$0")
_PROMPT2 = log.ansicol("$30$1:>$0")
